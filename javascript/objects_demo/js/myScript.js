let person = {
    name: 'Mital',
    age: 24,
};

// Dot Notation
person.name = 'John';

// Bracket Notation
let selection = 'name';
person[selection] = 'Mary';

console.log(person.name);