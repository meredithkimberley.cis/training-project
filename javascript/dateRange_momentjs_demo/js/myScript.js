function getDaterange(start, end, arr) {
  if (!moment(start).isSameOrAfter(end)) {
    if (arr.length == 0)
      arr.push(moment(start).format("dddd, MMMM Do YYYY, h:mm:ss a"));
    var next = moment(start).add(1, "d");
    arr.push(next.format("dddd, MMMM Do YYYY, h:mm:ss a"));
    getDaterange(next, end, arr);
  } else {
    return arr;
  }
  return arr;
}
var a = getDaterange("2014-05-01", "2014-05-16", []);
var tr = "";
for (var i = 0; i < a.length; i++) {
  tr += "<tr><td>" + a[i] + "</td></tr>";
}
document.getElementById("datedetails").innerHTML = tr;